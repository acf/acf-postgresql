local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")
db = require("acf.db")
dbmodelfunctions = require("dbmodelfunctions")
posix = require("posix")

-- Set variables
local confdfile = "/etc/conf.d/postgresql"
local conffile = "postgresql.conf"
local processname = "postgresql"
local packagename = "postgresql"

local path = "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin "

local datadirectory
local filelist
local confcontent

function mymodule.set_processname(p)
	processname = p
	confdfile = "/etc/conf.d/"..processname
end

-- ################################################################################
-- LOCAL FUNCTIONS

local determinedatadirectory = function()
	-- First check the conf.d file for file_dir, then PGDATA
	if not datadirectory then
		local conf = fs.read_file(confdfile) or ""
		datadirectory = format.get_ini_entry(conf, "", "data_dir")
		if not datadirectory or datadirectory == "" then
			datadirectory = format.get_ini_entry(conf, "", "PGDATA")
		end
		if datadirectory == "" then
			datadirectory = nil
		end
	end
	-- Otherwise, default to /var/lib/postgresql/$version/data where version is parsed from 'psql -V'
	if not datadirectory then
		local code, cmdresult = subprocess.call_capture({"psql", "-V"})
		datadirectory = "/var/lib/postgresql/" .. (string.match(cmdresult or "", "([%d.]+)%.%d") or "") .. "/data"
	end
end

local determinefilelist = function()
	if not filelist then
		determinedatadirectory()
		filelist = fs.find_files_as_array(".*%.conf", datadirectory)
		-- Newer versions of the postgresql package are set up with symlinks
		for i,name in ipairs(filelist) do
			if fs.is_link(name) then
				filelist[i] = posix.readlink(name)
			end
		end
	end
	return filelist
end

local getconfvalue = function(param)
	determinedatadirectory()
	local conffilepath = datadirectory.."/"..conffile
	if fs.is_link(conffilepath) then
		conffilepath = posix.readlink(conffilepath)
	end
	confcontent = confcontent or fs.read_file(conffilepath) or ""
	conftable = conftable or format.parse_configfile(confcontent)
	local val
	if conftable then
		val = conftable[param]
	end
	if val then
		-- Remove starting '=', if present
		val = string.match(val, "[ =]*(.*)")
		-- Remove "'"
		val = string.match(val, "[^']+")
	end
	return val
end

local determineconnection = function()
	-- Determine the connection parameters from the config files
	local listen_addresses = getconfvalue("listen_addresses")
	if not listen_addresses or listen_addresses == "" or listen_addresses == "*" or listen_addresses == "localhost" then
		listen_addresses = ""
	elseif string.find(listen_addresses, "'") then
		listen_addresses = string.match(listen_addresses, "[^']+")
	end
	local port = getconfvalue("port")
	if not port then port = "5432" end
	return db.create(db.engine.postgresql, nil, nil, nil, listen_addresses, port)
end

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
	return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
	return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "Postgresql Status")
end

function mymodule.getstatusdetails()
	return cfe({ type="longtext", value="", label="Postgresql Status Details" })
end

function mymodule.getfilelist()
	local listed_files = {}

	for i,name in ipairs(determinefilelist()) do
		local filedetails = posix.stat(name) or {}
		filedetails.filename = name
		table.insert(listed_files, filedetails)
	end

	table.sort(listed_files, function (a,b) return (a.filename < b.filename) end )

	return cfe({ type="structure", value=listed_files, label="Postgresql File List" })
end

function mymodule.getfiledetails(filename)
	return modelfunctions.getfiledetails(filename, determinefilelist())
end

function mymodule.updatefiledetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, determinefilelist())
end

function mymodule.get_logfile(self, clientdata)
	local retval = cfe({ type="structure", value={}, label="Log File Configuration" })

	determinedatadirectory()

	if string.find(getconfvalue("log_destination") or "", "syslog") then
		retval.value[#retval.value+1] = {facility=getconfvalue("syslog_facility") or "local0", grep=getconfvalue("syslog_ident") or "postgres"}
	end

	if getconfvalue("logging_collector") == "on" then
		local logdir = getconfvalue("log_directory") or "pg_log"
		if not string.find(logdir, "^/") then
			logdir = datadirectory.."/"..logdir
		end
		local logfile = getconfvalue("log_filename") or "postgresql-"
		-- drop the escapes
		logfile = string.match(logfile, "^[^%%]*")
		local files = fs.find_files_as_array(format.escapemagiccharacters(logfile)..".*", logdir)
		for i,f in ipairs(files) do
			retval.value[#retval.value+1] = {filename=f}
		end
	end

	if #retval.value == 0 then
		retval.value[#retval.value+1] = {facility="daemon", grep="postgres"}
	end

	retval.value[#retval.value+1] = {filename=datadirectory.."/postmaster.log"}

	return retval
end

for n,f in pairs(dbmodelfunctions) do
	mymodule[n] = function(...)
		return f(determineconnection, ...)
	end
end

return mymodule
