local mymodule = {}

-- Use on_load function to handle multiple instances of this ACF (i.e. postgresql2)
mymodule.mvc = {}
mymodule.mvc.on_load = function(self, parent)
	self.model.set_processname(string.match(self.conf.prefix, "[^/]+"))
end

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.details(self)
	return self.model.getstatusdetails()
end

function mymodule.listfiles(self)
	return self.model.getfilelist()
end

function mymodule.expert(self)
	return self.handle_form(self, function() return self.model.getfiledetails(self.clientdata.filename) end, self.model.updatefiledetails, self.clientdata, "Save", "Edit Postgresql File", "File Saved")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

-- Use acf-db to allow editing of the database
dbcontrollerfunctions = require("dbcontrollerfunctions")
for n,f in pairs(dbcontrollerfunctions) do
	mymodule[n] = f
end

return mymodule
