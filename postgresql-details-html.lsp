<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% viewlibrary.dispatch_component("status") %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% htmlviewfunctions.displayitem(data) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
